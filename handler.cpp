/*****************************************************
Title:			ai.cpp
Autor:			Walee Ahmed
Created on:		May 15, 2014
Description:	create trees of evolution
Purpose:		handler parser's output and creates tress
				using node class

******************************************************/
#include "handler.h"

handler::handler(){
	root = NULL;
}

handler::handler(string file){
	parser parse(file);
	set_data(parse.get_dictionary());
	create_trees();
	postorder(root);
	cout << ")"<<endl;
}

void handler::set_data(map<double, string> in){
	data = in;
	root = NULL;
}

typedef std::map<double, string>::iterator it_type;
void handler::setup(){
	for(it_type iterator = data.begin(); iterator != data.end(); ++iterator){
		//cout << it->first << " " << it->second <<endl;
		node *temp = new node(iterator->first, iterator->second);
		our_data.push(temp);
	}
}

void handler::create_trees(){
	setup();
	//cout << "queue size: " << our_data.size() << endl;
	
	double distance, distance2;
	do{
		//cout << "queue size before pop: " << our_data.size() << endl;
		//cout << "Date on the front " <<our_data.front()->get_value() <<endl;
		node *temp = our_data.front();
		our_data.pop();
		//cout << "Date on the front " <<our_data.front()->get_value() <<endl;
		node *temp2= our_data.front();
		our_data.pop();
		//cout << "got the first 2 from queue\n";
		//cout << "queue size: " << our_data.size() << endl;

		distance = (temp->get_value() - temp2->get_value() );
		//cout << "distance 1\n";
		if (distance < 0) distance *= -1;

		//cout << "between\n";
		
		if(!our_data.empty()){
			distance2 = temp2->get_value()-our_data.front()->get_value();
			//cout << "distance 1\n";
			if (distance2 < 0) distance2 *= -1;
			//cout << "calculated distances\n";
		}
		if (distance2 < distance ){
			our_data.push(temp);
			temp = our_data.front();
			our_data.pop();
			creator(temp2, temp);
			}
		else{
			//cout << temp->get_value() << " " << temp2->get_value() <<endl;
			creator(temp,temp2);
		}	
	}while(our_data.size() > 1);

	root = our_data.front();
}

void handler::creator(node *left, node *right){
	node *temp = new node();
	//temp = right->AveragedNode(left);
	right->AveragedNode(left, temp);
	temp->setChildren(left, right);
	our_data.push(temp);
}

void handler::test(){
	for(node *temp = root; temp!=NULL; temp=temp->getleft()){
		cout << "animal " << temp->get_name() << " value " << temp->get_value() << endl;
	}
	for(node *temp = root; temp!=NULL; temp=temp->getright()){
		cout << "animal " << temp->get_name() << " value " << temp->get_value() << endl;
	}
}

void handler::postorder(node* p){ 
   	if(p != NULL){
    	if(p->getleft()) {
    		cout << "(";
    		postorder(p->getleft());
    		cout << ", ";
    	}
    	if(p->getright()){ 
    		postorder(p->getright());
    		cout << ")";
     	}
     	if (p->get_name() != "") cout<<p->get_name() << " " <<p->get_value();
   		//if(p != NULL) cout << ")";
   }
   else return;
}