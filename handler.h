/*****************************************************
Title:			ai.cpp
Autor:			Walee Ahmed
Created on:		May 15, 2014
Description:	create trees of evolution
Purpose:		handler parser's output and creates tress
				using node class

******************************************************/

#ifndef HANDLER_H
#define HANDLER_H

#include <iostream>
#include <map>
#include <fstream>
#include <string>
#include <queue>

#include "parser.h"
#include "node.h"

using namespace std;

class handler{
public:
	handler();
	handler(string file); //takes the name of the file as contructor
	void set_data(map<double, string>); //manually set all data
	void create_trees(); //creates the tree with the data
	void test(); //to check the tree 
	void postorder(node* p); //prints out the tree
private:
	map<double, string> data; //stores data of animals
	node *root; //main toor of the tree
	queue<node*> our_data; //queue used to create tree
	void setup(); //creating nodes from map and putting them into queue
	void creator(node *left, node *right); //creates new nodes and pushes in queue
};
#endif