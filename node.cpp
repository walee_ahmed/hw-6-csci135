/*****************************************************
Title:			ai.cpp
Autor:			Walee Ahmed
Created on:		May 15, 2014
Description:	create trees of evolution
Purpose:		nodes of thee tree

******************************************************/

#include "node.h"
#include <cmath>

node::node(double value, string name){
	set_data(value,name);
}

node::node(){

}

void node::set_data(double value, string name){
	this-> value = value;
	this->name = name;
	left = NULL;
	right = NULL;
}

void node::set_data(double value){
	this-> value = value;
	this->name = name;
	left = NULL;
	right = NULL;
}

double node::get_value(){
	return value;
}

string node::get_name(){
	return name;
}

double node::Nodedifference(node *compare){
	return abs(value-compare->get_value());
}

void node::AveragedNode(node* in, node* &out){
	//node *temp = new node();
	out->set_data((value+in->get_value())/2);
	//return temp; 
}

void node::setChildren(node *l, node *r){
	left = l;
	right = r;
}

node* node::getleft(){
	return left;
}

node* node::getright(){
	return right;
}


