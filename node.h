/*****************************************************
Title:			ai.cpp
Autor:			Walee Ahmed
Created on:		May 15, 2014
Description:	create trees of evolution
Purpose:		nodes of thee tree

******************************************************/
#ifndef NODE_H
#define  NODE_H

#include <iostream>
#include <map>
#include <string>

using namespace std;

class node{
public:
	node();
	node(double value, string name);//make node with data inputs using construtor
	void set_data(double value); //settinf data manually
	void set_data(double value, string name); //settinf data manually
	double get_value(); 
	string get_name();
	double Nodedifference(node*); //value difference between two nodes
	void AveragedNode(node* in, node* &out); //average of two nodes and returns new node by reference
	void setChildren(node *, node *); //setting up nodes children
	node* getleft(); //get left children
	node* getright(); //get right children
private:
	double value;
	string name;
	node *left;
	node *right;
};

#endif