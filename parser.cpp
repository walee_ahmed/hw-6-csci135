/*****************************************************
Title:          ai.cpp
Autor:          Walee Ahmed
Created on:     May 15, 2014
Description:    create trees of evolution
Purpose:        parses input file

******************************************************/
#include "parser.h"
#include <sstream>
#include <stdlib.h>
#include <ctype.h>

using namespace std;
parser::parser(){
    
}

parser::parser(string Filename){
    //cout << "Start Parse....\n";
    parseFile(Filename);
}

map<double, string> parser::parseFile(string Filename){
    //cout << "Works2\n";
    startParse(Filename);
    map<double, string>temp = get_dictionary();
    return temp;
}

map<double, string> parser::get_dictionary(){
    //cout << "Works3\n";
    return dictionary;
}

void parser::startParse(string Filename){
    //cout << "Works4\n";
    ifstream inputfile(Filename.c_str());
    if (!inputfile){
        cout << "ERRROR: File does not exit!\n";
        exit(1);
    }
    string holder, holder2, name;
    int count;
    double value=0;
    while(getline(inputfile, holder)){
        stringstream spliter(holder);
        count = 0;
        //cout << "splitter: "<<spliter.str().length() <<endl;
        //splits the input lines from the file into the number and string
        while(spliter >> holder2){
            //if (!isdigit(holder2.c_str()[2])) cout << value<<" We got a number\n";
            //if (count > 2) goto error;
            if (count == 0){
                name = holder2;
                count++;
            }
            else{
                //cout << value << endl;
                value = atof(holder2.c_str());
                if(value < 0) continue;
                count++;
                //stores into the map using the value as key and string as data 
                //dictionary[value] = name;
            }
            //error:
            //cout << "ERROR in line\n";
            //break;
        }
        if (count == 2) dictionary[value] = name;

    }
}

typedef std::map<double, string>::iterator it_type;
void parser::print(){
    for(it_type iterator = dictionary.begin(); iterator != dictionary.end(); ++iterator){
        cout << iterator->first << " " << iterator->second <<endl;
    }
}