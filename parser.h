/*****************************************************
Title:          ai.cpp
Autor:          Walee Ahmed
Created on:     May 15, 2014
Description:    create trees of evolution
Purpose:        parses input file

******************************************************/
#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

class parser{
    public:
        parser(); //Does noting
        parser(string Filename = "input.txt"); //constructer takes the input file as a parameter
        map<double, string> parseFile(string Filename); //use this if constuctor not used to input file 
        map<double, string> get_dictionary(); //return the dictionary of animal names and their value
        void print(); //prints out the dictionary
        
    private:
        map<double, string> dictionary; //stores animals and their value here
        void startParse(string Filename); //the main parsing function 
};

#endif