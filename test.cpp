/*****************************************************
Title:			ai.cpp
Autor:			Walee Ahmed
Created on:		May 15, 2014
Description:	create trees of evolution
Purpose:		driver program

******************************************************/

#include "parser.h"
#include <map>
#include <string>
#include "handler.h"

using namespace std;

void print_map(map<double, string> s);

int main(int argc, const char *argv[]){
	//cout << argc << " WORKS!\n";
	if (argc > 1 && argc <3){
		string file = argv[1];
		//cout << file << endl;
		handler h(file); 
		//h.test();	
	}
	else{
		cout << "Tryng to use default input.txt file\n";
		handler h("input.txt");
	}
	   
}

